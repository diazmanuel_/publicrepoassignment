import serial

ser = serial.Serial ("/dev/serial0",
	baudrate = 38400,
	bytesize = serial.EIGHTBITS,
	parity = serial.PARITY_NONE,
	stopbits = serial.STOPBITS_ONE)


temp = 0.0
while(1):
	text = ser.readline().decode('utf-8')
	try:
		temp += float(text)
	except ValueError:
		print()
	print('text:',text)
	print('temp:',temp)
